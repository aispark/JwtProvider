package com.albsoft.jwt;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;


/**
 * "/login" 과 "/user" 를 테스트 합니다.
 * 
 * @author park
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class JwtProviderMvcTests {
	@Autowired
	private MockMvc mvc;

	@Test
	public void contextLoads() throws Exception {
		// "/login" 은 request method가 "post" 인지와  contentType이 "application/json;charset=UTF-8"인 request만 허용 됩니다.
		String token = this.mvc.perform(post("/login").content("{\"id\": \"aispark\", \"password\": \"1234\"}").contentType("application/json;charset=UTF-8")).andDo(print())
		.andReturn().getResponse().getHeader("Jwt-Header");

		// 생성된 토큰값을 확인 합니다.
		this.mvc.perform(get("/user").header("Jwt-Header", token).contentType("application/json;charset=UTF-8")).andDo(print());
	}
}
