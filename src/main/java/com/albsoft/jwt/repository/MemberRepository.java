package com.albsoft.jwt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.albsoft.jwt.domain.Member;

@Repository
public interface MemberRepository extends JpaRepository<Member, String> {

}
