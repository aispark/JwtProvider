package com.albsoft.jwt.auth.ajax.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.albsoft.jwt.domain.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.AccessDeniedException;

/**
 * 로그인에 대한 필터링는 URL이 LOGIN_END_POINT 이면서 Method가 POST인 request만 가능하게 합니다.
 * 해당 해당 필터에서 ContentType 이 application/json;charset=utf-8 인지 여부를 판단합니다.
 * @author park
 *
 */
public class AjaxAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

	private final ObjectMapper objectMapper;

	public AjaxAuthenticationFilter(RequestMatcher requestMatcher, ObjectMapper objectMapper) {
		super(requestMatcher);
		this.objectMapper = objectMapper;
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException {
		if (isJson(request)) {
			Member member = objectMapper.readValue(request.getReader(), Member.class);
			UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(member.getId(), member.getPassword());
			return getAuthenticationManager().authenticate(authentication);
		} else {
			throw new AccessDeniedException("Don't use content type for " + request.getContentType());
		}
	}

	private boolean isJson(HttpServletRequest request) {
		return MediaType.APPLICATION_JSON_UTF8_VALUE.equalsIgnoreCase(request.getContentType());
	}
}
