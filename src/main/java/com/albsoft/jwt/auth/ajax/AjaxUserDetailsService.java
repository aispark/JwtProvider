package com.albsoft.jwt.auth.ajax;

import com.albsoft.jwt.auth.UserDetailsImpl;
import com.albsoft.jwt.domain.Member;
import com.albsoft.jwt.repository.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

/**
 * DB 조회 및 token에 대한 유효형 판단을 처리하는 클래스 입니다.
 * 
 * @author park
 *
 */
@Component
public class AjaxUserDetailsService implements UserDetailsService {

	@Autowired
	private MemberRepository repository;

	/**
	 * MemberRepository에서 해당 ID 정보를 가져옵니다.
	 */
	@Override
	public UserDetails loadUserByUsername(String username) {
		Member user = repository.findById(username).orElse(null);

		if (user == null) {
			throw new UsernameNotFoundException(username + "라는 사용자가 없습니다.");
		}

		return new UserDetailsImpl(user, AuthorityUtils.createAuthorityList(user.getRole()));
	}
}
