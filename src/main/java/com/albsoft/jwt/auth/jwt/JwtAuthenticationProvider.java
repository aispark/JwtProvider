package com.albsoft.jwt.auth.jwt;

import com.albsoft.jwt.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

/**
 * 인증 및 권한 제어를 제공하는 클래스입니다.
 * filter로 부터 받은 cerdentials(token)를 userDetailsService에 넘겨줍니다.
 * token 정보에 대한 별도의 인증 처리를 추가할 수 있습니다.
 * 
 * @author park
 *
 */
@Component
public class JwtAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	private JwtUserDetailsService userDetailsService;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		if (authentication.getCredentials() == null) {
			throw new BadCredentialsException("Bad token");
		}

		String token = authentication.getCredentials().toString();

		if (JwtUtil.verify(token)) {
			UserDetails userDetails = userDetailsService.loadUserByUsername(token);
			return new JwtAuthenticationToken(userDetails.getUsername(), userDetails.getPassword(), userDetails.getAuthorities());
		} else {
			throw new BadCredentialsException("Bad token");
		}
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return JwtAuthenticationToken.class.isAssignableFrom(authentication);
	}
}
