package com.albsoft.jwt.auth.jwt;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.albsoft.jwt.auth.UserDetailsImpl;
import com.albsoft.jwt.domain.Member;
import com.albsoft.jwt.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * token에 대한 유효형 판단을 합니다.
 * auth0에서 제공하는 JWT.verify()는 내부적으로 decode(), 알고리즘 검사, 날짜 유효성, claims 검사를 합니다.
 * 
 * @author park
 *
 */
@Component
public class JwtUserDetailsService implements UserDetailsService {

	@Override
	public UserDetails loadUserByUsername(String token) {
		if(!JwtUtil.verify(token)) {
            throw new BadCredentialsException("Not used Token");
        }

		DecodedJWT decodedJWT = JwtUtil.tokenToJwt(token);

		if (decodedJWT == null) {
			throw new BadCredentialsException("Not used Token");
		}

		String id = decodedJWT.getClaim("id").asString();
		String role = decodedJWT.getClaim("role").asString();

		return new UserDetailsImpl(id, AuthorityUtils.createAuthorityList(role));
	}
}
