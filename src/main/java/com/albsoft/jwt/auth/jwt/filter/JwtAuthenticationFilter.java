package com.albsoft.jwt.auth.jwt.filter;

import com.albsoft.jwt.auth.jwt.JwtAuthenticationToken;
import com.albsoft.jwt.auth.jwt.JwtInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.util.StringUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 토큰이 필요한 필터입니다.
 * Token에 대한 인증이 필요없는 LOGIN_END_POINT와 TOKEN_END_POINT 를 제외할 Custom RequestMatcher를 만들어줍니다.
 * OrRequestMatcher를 이용하면 여러개의 RequestMatcher를 필터링 할 수 있습니다.
 * 
 * @author park
 *
 */
public class JwtAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

	public JwtAuthenticationFilter(RequestMatcher requestMatcher) {
		super(requestMatcher);
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request,
	                                            HttpServletResponse response) throws AuthenticationException {
		String token = request.getHeader(JwtInfo.HEADER_NAME);
		
		//header에 token 정보가 담아있는지 판별합니다. 
		if (StringUtils.isEmpty(token)) {
			throw new AccessDeniedException("Not empty Token");
		} else {
			return getAuthenticationManager().authenticate(new JwtAuthenticationToken(token));
		}
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request,
	                                        HttpServletResponse response,
	                                        FilterChain chain,
	                                        Authentication authResult) throws IOException, ServletException {
		SecurityContext context = SecurityContextHolder.createEmptyContext();
		context.setAuthentication(authResult);
		SecurityContextHolder.setContext(context);
		chain.doFilter(request, response);
	}

	@Override
	protected void unsuccessfulAuthentication(HttpServletRequest request,
	                                          HttpServletResponse response,
	                                          AuthenticationException failed) throws IOException, ServletException {
		SecurityContextHolder.clearContext();
		getFailureHandler().onAuthenticationFailure(request, response, failed);
	}
}
