Jwt 기능을 간단하게 구현한 프로젝트 입니다.

거의 대부분 [Spring Boot - Security + JWT](http://heowc.tistory.com/46)을 참조하였습니다.

## 설정방법

git 설치 되어 있어야 합니다.

`프로젝트 clone` 하는 방법:

```bash
$ cd workspace 
$ git clone https://gitlab.com/aispark/JwtProvider.git
```